# davek1312/serialise

Serialise/Deserialise objects using the `jms/serializer` package.


# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/serialise),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/serialise
```


# Configuration

Add the traits to the model you want to serialise/deserialise
```php
use Davek1312\Serialise\Traits\Serialise;
use Davek1312\Serialise\Traits\Deserialise;
use Davek1312\Serialise\Traits\Common;
```

Or extend from the base serialise model that uses these traits  
```php
class YourClass extends Davek1312\Serialise\Models\BaseSerialiseModel
```

Annotate your model using the [JMS annotations](http://jmsyst.com/libs/serializer/master/reference/annotations).

# Usage

## Serialise
```php
#JSON
->serialiseToJson();
->serialise(Utils::DATA_TYPE_JSON);

#XML
->serialiseToXml();
->serialise(Utils::DATA_TYPE_XML);

#Array
->serialiseToArray();
->serialise(Utils::DATA_TYPE_ARRAY);

#YAML
->serialiseToYaml();
->serialise(Utils::DATA_TYPE_YAML);

#Errors
->hasSerialiseError();
->getSerialiseException();
```

## Deserialise
```php
#JSON
::deserialiseJson();
::deserialise($json, Utils::DATA_TYPE_JSON);

#XML
::deserialiseXml();
::deserialise($xml, Utils::DATA_TYPE_XML);

#Array
::deserialiseArray();
::deserialise($array, Utils::DATA_TYPE_ARRAY);

#Errors
->hasDeserialiseError();
->getDeserialiseException();
```