<?php

namespace Davek1312\Serialise\Traits;

use Davek1312\Serialise\Utils;
use JMS\Serializer\Annotation\Exclude;
use ReflectionClass;
use Exception;

/**
 * Trait that deserialises data into an object
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait Deserialise {

    /**
     * @var Exception
     *
     * @Exclude()
     */
    private $deserialiseException;

    /**
     * Returns an object based on $data
     *
     * @param string|array $data    string OR array.
     * @param string $dataType      'xml' OR 'json' OR 'array'
     *
     * @return object returned object is of class get_called_class()
     */
    public static function deserialise($data, $dataType) {
        try {
            if($dataType == Utils::DATA_TYPE_ARRAY) {
                $dataType = Utils::DATA_TYPE_JSON;
                $data = json_encode($data);
            }
            $serialiser = Utils::getSerialiser();
            return $serialiser->deserialize($data, get_called_class(), $dataType);
        }
        catch(Exception $e) {
            return static::getObjectWithDeserialiseException($e);
        }
    }

    /**
     * Returns an object of get_called_class() and sets $deserialiseException
     *
     * @param Exception $e
     *
     * @return object returned object is of class get_called_class()
     */
    public static function getObjectWithDeserialiseException(Exception $e) {
        $reflector = new ReflectionClass(get_called_class());
        $object = $reflector->newInstanceWithoutConstructor();
        $object->setDeserialiseException($e);
        return $object;
    }

    /**
     * Returns a JSON string as an object
     *
     * @param string $json
     *
     * @return object
     */
    public static function deserialiseJson($json) {
        return static::deserialise($json, Utils::DATA_TYPE_JSON);
    }

    /**
     * Returns an XML string as an object
     *
     * @param string $xml
     *
     * @return object
     */
    public static function deserialiseXml($xml) {
        return static::deserialise($xml, Utils::DATA_TYPE_XML);
    }

    /**
     * Returns an array as an object
     *
     * @param array $array
     *
     * @return object
     */
    public static function deserialiseArray(array $array) {
        return static::deserialise(json_encode($array), Utils::DATA_TYPE_JSON);
    }

    /**
     * @return Exception
     */
    public function getDeserialiseException() {
        return $this->deserialiseException;
    }

    /**
     * @param Exception $deserialiseException
     */
    public function setDeserialiseException($deserialiseException) {
        $this->deserialiseException = $deserialiseException;
    }

    /**
     * @return boolean
     */
    public function hasDeserialiseError() {
        return $this->deserialiseException !== null;
    }
}