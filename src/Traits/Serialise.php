<?php

namespace Davek1312\Serialise\Traits;

use Davek1312\Serialise\Utils;
use JMS\Serializer\SerializationContext;
use Exception;
use JMS\Serializer\Annotation\Exclude;

/**
 * Trait that serialises an object
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait Serialise {

    /**
     * @var Exception
     *
     * @Exclude()
     */
    private $serialiseException;

    /**
     * Returns the object in the format of $dataType
     *
     * @param string $dataType  'xml' OR 'json' OR 'array' or 'yml'
     *
     * @return string|array|null
     */
    public function serialise($dataType) {
        try {
            if($dataType == Utils::DATA_TYPE_ARRAY) {
                return $this->serialiseToArray();
            }
            $serialiser = Utils::getSerialiser();
            $context = new SerializationContext();
            $context->setSerializeNull(true);
            return $serialiser->serialize($this, $dataType, $context);
        }
        catch(Exception $e) {
            $this->serialiseException = $e;
        }
        return null;
    }

    /**
     * Returns the object as a JSON string
     *
     * @return string
     */
    public function serialiseToJson() {
        return $this->serialise(Utils::DATA_TYPE_JSON);
    }

    /**
     * Returns the object as an XML string
     *
     * @return string
     */
    public function serialiseToXml() {
        return $this->serialise(Utils::DATA_TYPE_XML);
    }

    /**
     * Returns the object as an array
     *
     * @return array
     */
    public function serialiseToArray() {
        return json_decode($this->serialiseToJson(), true);
    }

    /**
     * Returns the object as an YAML string
     *
     * @return array
     */
    public function serialiseToYaml() {
        return $this->serialise(Utils::DATA_TYPE_YAML);
    }

    /**
     * Returns any exception thrown in the serialise method
     *
     * @return Exception
     */
    public function getSerialiseException() {
        return $this->serialiseException;
    }

    /**
     * @param Exception $serialiseException
     */
    public function setSerialiseException($serialiseException) {
        $this->serialiseException = $serialiseException;
    }

    /**
     * @return boolean
     */
    public function hasSerialiseError() {
        return $this->serialiseException !== null;
    }
}