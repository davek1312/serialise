<?php

namespace Davek1312\Serialise\Traits;

use Exception;

/**
 * Common serialisation/deserialisation methods
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait Common {

    /**
     * Returns the value from an array of nested object accessors. Will return null if the nested value does not exist.
     *
     * @param array $objectAccessors
     *
     * @return mixed
     */
    public function getSubelement(array $objectAccessors) {
        if(count($objectAccessors) > 0) {
            try {
                $element = $this;
                foreach($objectAccessors as $objectAccessor) {
                    if($element !== null) {
                        if(method_exists($element, $objectAccessor)) {
                            $element = $element->{$objectAccessor}();
                        }
                        else {
                            return null;
                        }
                    }
                    else {
                        return null;
                    }
                }
                return $element;
            }
            catch(Exception $e) {
                return null;
            }
        }
        return null;
    }
}