<?php

namespace Davek1312\Serialise;

use JMS\Serializer\SerializerBuilder;
use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\Serializer;

/**
 * Helpful methods
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Utils {

    /**
     * @var string
     */
    const DATA_TYPE_JSON = 'json';
    /**
     * @var string
     */
    const DATA_TYPE_XML = 'xml';
    /**
     * @var string
     */
    const DATA_TYPE_ARRAY = 'array';
    /**
     * @var string
     */
    const DATA_TYPE_YAML = 'yml';

    /**
     * Creates a new JMS\Serializer\Serializer
     *
     * @return Serializer
     */
    public static function getSerialiser() {
        AnnotationRegistry::registerLoader('class_exists');
        return SerializerBuilder::create()->build();
    }
}