<?php

namespace Davek1312\Serialise\Models;
use Davek1312\Serialise\Traits\Common;
use Davek1312\Serialise\Traits\Deserialise;
use Davek1312\Serialise\Traits\Serialise;


/**
 * Uses all the serialise traits
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class BaseSerialiseModel {

    use Serialise;
    use Deserialise;
    use Common;
}