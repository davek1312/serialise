<?php

namespace Davek1312\Serialise\Tests\Models;

use Davek1312\Serialise\Models\BaseSerialiseModel;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\XmlElement;
use JMS\Serializer\Annotation\Exclude;

/**
 * @XmlRoot("Person")
 */
class Person extends BaseSerialiseModel {

    const NAME = 'david';
    const OBJECT_AS_ARRAY   = ['name' => Person::NAME];
    const OBJECT_AS_JSON    = '{"name":"'.Person::NAME.'"}';
    const OBJECT_AS_XML     = '<?xml version="1.0" encoding="UTF-8"?><Person>  <name>david</name></Person>';
    const OBJECT_AS_YAML    = 'name: david';

    /**
     * @Type("string")
     * @XmlElement(cdata=false)
     */
    public $name;
    /**
     * @Exclude()
     */
    public $person;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getPerson() {
        return $this->person;
    }

    public function setPerson($person) {
        $this->person = $person;
    }
}