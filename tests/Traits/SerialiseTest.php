<?php

namespace Davek1312\Serialise\Tests\Traits;

use Davek1312\Serialise\Utils;
use Davek1312\Serialise\Tests\Models\Person;

class SerialiseTest extends \PHPUnit_Framework_TestCase {

    private $person;

    public function setUp() {
        parent::setUp();
        $this->person = new Person();
        $this->person->setName(Person::NAME);
    }

    public function testSerialise() {
        $this->assertValidSerialise($this->person->serialise(Utils::DATA_TYPE_JSON), Person::OBJECT_AS_JSON);
        $this->assertValidSerialise($this->removeLineEndings($this->person->serialise(Utils::DATA_TYPE_XML)),  $this->removeLineEndings(Person::OBJECT_AS_XML));
        $this->assertValidSerialise($this->person->serialise(Utils::DATA_TYPE_ARRAY), Person::OBJECT_AS_ARRAY);
        $this->assertValidSerialise($this->removeLineEndings($this->person->serialise(Utils::DATA_TYPE_YAML)), $this->removeLineEndings(Person::OBJECT_AS_YAML));
        $this->assertInValidSerialise($this->person->serialise(null));
        $this->assertInValidSerialise($this->person->serialise(''));
    }

    public function testSerialiseToJson() {
        $this->assertValidSerialise($this->person->serialiseToJson(), Person::OBJECT_AS_JSON);
    }

    public function testSerialiseToXml() {
        $this->assertValidSerialise($this->removeLineEndings($this->person->serialiseToXml()),  $this->removeLineEndings(Person::OBJECT_AS_XML));
    }

    public function testSerialiseToArray() {
        $this->assertValidSerialise($this->person->serialiseToArray(), Person::OBJECT_AS_ARRAY);
    }

    public function testSerialiseToYaml() {
        $this->assertValidSerialise($this->removeLineEndings($this->person->serialiseToYaml()), $this->removeLineEndings(Person::OBJECT_AS_YAML));
    }

    //Different line endings in windows/linux
    private function removeLineEndings($xml) {
        return  preg_replace('/\r|\n/', '', $xml);
    }

    private function assertValidSerialise($actualSerialiseObject, $expectedSerialiseObject) {
        $this->assertNull($this->person->getSerialiseException());
        $this->assertNotNull($actualSerialiseObject);
        $this->assertSame($expectedSerialiseObject, $actualSerialiseObject);
    }

    private function assertInValidSerialise($serialisedObject) {
        $this->assertNotNull($this->person->getSerialiseException());
        $this->assertNull($serialisedObject);
    }

    public function testHasSerialiseError() {
        $person = new Person();
        $this->assertFalse($person->hasSerialiseError());

        $person->setSerialiseException(new \Exception());
        $this->assertTrue($person->hasSerialiseError());
    }
}