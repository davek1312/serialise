<?php

namespace Davek1312\Serialise\Tests\Traits;

use Davek1312\Serialise\Tests\Models\Person;

class CommonTest extends \PHPUnit_Framework_TestCase {

    public function testGetSubelement() {
        $nestedName = 'Kelly';
        $nestedPerson = new Person();
        $nestedPerson->setName($nestedName);

        $name = 'David';
        $person = new Person();
        $person->setName($name);
        $person->setPerson($nestedPerson);

        $this->assertNull($person->getSubelement([]));
        $this->assertNull($person->getSubelement(['getNonExistantProperty']));
        $this->assertNull($person->getSubelement(['getName', 'getName']));

        $this->assertSame($name, $person->getSubelement(['getName']));
        $this->assertSame($nestedPerson, $person->getSubelement(['getPerson']));
        $this->assertSame($nestedName, $person->getSubelement(['getPerson', 'getName']));
    }
}