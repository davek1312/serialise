<?php

namespace Davek1312\Serialise\Tests\Traits;

use Davek1312\Serialise\Utils;
use Davek1312\Serialise\Tests\Models\Person;

class DeserialiseTest extends \PHPUnit_Framework_TestCase {

    private $person;

    public function setUp() {
        parent::setUp();
        $this->person = new Person();
    }

    public function testDeserialise() {
        $this->assertValidDeserialise(Person::deserialise(Person::OBJECT_AS_JSON, Utils::DATA_TYPE_JSON));
        $this->assertValidDeserialise(Person::deserialise(Person::OBJECT_AS_XML, Utils::DATA_TYPE_XML));
        $this->assertValidDeserialise(Person::deserialise(Person::OBJECT_AS_ARRAY, Utils::DATA_TYPE_ARRAY));
        $this->assertInValidDeserialise(Person::deserialise(Person::OBJECT_AS_JSON, Utils::DATA_TYPE_XML));
        $this->assertInValidDeserialise(Person::deserialise(null, Utils::DATA_TYPE_XML));
        $this->assertInValidDeserialise(Person::deserialise('', Utils::DATA_TYPE_XML));
    }

    public function testDeserialiseJson() {
        $this->assertValidDeserialise(Person::deserialiseJson(Person::OBJECT_AS_JSON));
    }

    public function testDeserialiseXml() {
        $this->assertValidDeserialise(Person::deserialiseXml(Person::OBJECT_AS_XML));
    }

    public function testDeserialiseArray() {
        $this->assertValidDeserialise(Person::deserialiseArray(Person::OBJECT_AS_ARRAY));
    }

    private function assertValidDeserialise($object) {
        $this->assertNull($this->person->getDeserialiseException());
        $this->assertNotNull($object);
        $this->assertEquals(Person::NAME, $object->getName());
    }

    private function assertInValidDeserialise($object) {
        $exception = $object->getDeserialiseException();
        $this->assertNotNull($exception);
        $this->assertInstanceOf('Exception', $exception);
    }

    public function testHasDeserialiseError() {
        $person = new Person();
        $this->assertFalse($person->hasDeserialiseError());

        $person->setDeserialiseException(new \Exception());
        $this->assertTrue($person->hasDeserialiseError());
    }
}