<?php

namespace Davek1312\Serialise\Tests;

use Davek1312\Serialise\Utils;

class HelpersTest extends \PHPUnit_Framework_TestCase {

    public function testGetSerialiser() {
        $this->assertInstanceOf('JMS\Serializer\Serializer', Utils::getSerialiser());
    }
}